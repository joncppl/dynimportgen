from pycparserext.ext_c_parser import GnuCParser, FuncDeclExt
from pycparser import c_ast, c_generator
import subprocess
import re
import ctypes
import argparse


class FunctionDefinition:
    def __init__(self, ast_node: c_ast.Decl):
        self._ast_node = ast_node
        self._name = ast_node.name
        self._ptr_type = self._name + '_ptr_type'
        self._storage_name = self._name + '_storage'

    @property
    def name(self):
        return self._name

    @property
    def storage_name(self):
        return self._storage_name

    @property
    def ptr_type(self):
        return self._ptr_type

    def typedef(self):
        decl = self._ast_node
        ret = 'typedef '
        t = decl.type
        args = t.args
        rt = t.type
        ret += c_generator.CGenerator().visit(rt)
        while isinstance(rt, c_ast.PtrDecl):
            rt = rt.type
            ret += '*'
        ret += ' (* '
        ret += self._ptr_type
        ret += ') ('
        for i in range(len(args.params)):
            arg = args.params[i]
            ret += c_generator.CGenerator().visit(arg)
            if i < len(args.params) - 1:
                ret += ', '
        ret += ');'
        return ret

    def storage_definition(self):
        return f'{self._ptr_type} {self._storage_name};'

    def macro(self):
        r = f'#define {self._name} {self._storage_name}'
        return r

    def storage_declaration(self):
        return f'extern {self.storage_definition()}'


def gen_header(name, includes, functions):
    r = '#pragma once\n'
    for include in includes:
        r += f'#include <{include}>\n'

    r += """
#ifdef __cplusplus
extern "C" {
#endif

"""

    for function in functions:
        r += function.typedef() + '\n'
        r += function.storage_declaration() + '\n'
        r += function.macro() + '\n'

    r += '\n'

    r += f"""
typedef enum
{{
    {name}_load_notification_load_type_library,
    {name}_load_notification_load_type_function,
}} {name}_load_notification_load_type;

typedef enum
{{
    {name}_load_notification_load_status_success,
    {name}_load_notification_load_status_error,
}} {name}_load_notification_load_status;

typedef void (* {name}_load_notification)({name}_load_notification_load_type, {name}_load_notification_load_status, const char* ident);

typedef struct
{{
int lib_load_count;
int function_load_count;
}} {name}_load_return;

"""

    r += f'\nextern {name}_load_return {name}_load(const char** extra_lib_names, int* flags_ptr, {name}_load_notification);\n'
    r += f'extern void {name}_unload();\n'

    r += """
    
#ifdef __cplusplus
}
#endif
"""
    return r


def c_safe_lib_name(lib: str):
    return lib.replace('.', '_').replace('-', '_')


def gen_source(name, functions, libs, header_name):
    r = '#include <dlfcn.h>\n'
    r += '#include <stdlib.h>\n'
    r += f'#include "{header_name}"\n\n'

    for function in functions:
        r += function.storage_definition() + '\n'

    for lib in libs:
        r += f'static void* {c_safe_lib_name(lib)} = NULL;\n'

    r += 'static void** extra_libs = NULL;\n'
    r += 'static int num_libs = 0;\n'
    r += 'static int is_init = 0;\n'

    r += """
  static void* load_sym(char* sym)
  {
    void* f = NULL;
"""
    for lib in libs:
        r += f'  f = dlsym({c_safe_lib_name(lib)}, sym);\n'
        r += '  if (f) return f;\n'
    r += """
    
  for (int i = 0; i < num_libs; i++) {
    if (extra_libs[i]) {
      f = dlsym(extra_libs[i], sym);
      if (f) return f;
    }
  }
    
  return f;
  } 
"""

    r += '\n'
    r += f'{name}_load_return {name}_load(const char** extra_lib_names, int* flags_ptr, {name}_load_notification notify)\n'
    r += '{\n'

    r += f'  {name}_load_return result;\n'
    r += """
    result.lib_load_count = -1;
    result.function_load_count = -1;
  if (is_init)
    return result;
  is_init = 1;
  int lib_count = 0;
  int function_count = 0;
  int flags = RTLD_LAZY;
  if (flags_ptr) {
    flags = *flags_ptr;
  }
  num_libs = 0;
  if (extra_lib_names) {
    const char** p = extra_lib_names;
    while (*p) {
        p++;
        num_libs++;
    }
    extra_libs = (void**) malloc(sizeof(void*) * num_libs);
    memset(extra_libs, 0, sizeof(void*) * num_libs);
  }
"""
    for lib in libs:
        r += f'  {c_safe_lib_name(lib)} = dlopen("{lib}", flags);\n'
        r += f'  if ({c_safe_lib_name(lib)}) lib_count++;\n'
        r += f'  if (notify) notify({name}_load_notification_load_type_library, {c_safe_lib_name(lib)} ? {name}_load_notification_load_status_success : {name}_load_notification_load_status_error, "{lib}");\n\n'

    r += f"""
  for (int i = 0; i < num_libs; i++) {{
    extra_libs[i] = dlopen(extra_lib_names[i], flags);
    if (extra_libs[i])  lib_count++;
    if (notify) notify({name}_load_notification_load_type_library, extra_libs[i] ? {name}_load_notification_load_status_success : {name}_load_notification_load_status_error, extra_lib_names[i]);
  }}    
"""

    for function in functions:
        r += f'  {function.storage_name} = ({function.ptr_type}) load_sym("{function.name}");\n'
        r += f'  if ({function.storage_name}) {{\n'
        r += '    function_count++;\n'
        r += '  }\n'
        r += f' if (notify) notify({name}_load_notification_load_type_function, {function.storage_name} ? {name}_load_notification_load_status_success : {name}_load_notification_load_status_error, "{function.name}");\n\n'

    r += f'  result.lib_load_count = lib_count;\n'
    r += f'  result.function_load_count = function_count;\n'
    r += '  return result;\n'
    r += '}\n\n'

    r += f'void {name}_unload()\n'
    r += '{\n'
    r += '  if (!is_init) return;\n\n'
    for function in functions:
        r += f'  {function.storage_name} = NULL;\n'
    for lib in libs:
        r += f'  if ({c_safe_lib_name(lib)}) {{\n'
        r += f'    dlclose({c_safe_lib_name(lib)});\n'
        r += '  }\n\n'
    r += """
    for (int i = 0; i < num_libs; i++) {
      if (extra_libs[i]) {
          dlclose(extra_libs[i]);
      }
    }
    if (num_libs)
      free(extra_libs);
    num_libs = 0;
    is_init = 0;
"""
    r += "}\n\n"

    return r


def generate_file_to_parse(headers):
    fn = 'temp.h'
    f = open(fn, 'w')
    f.write('#define __attribute__(x)\n')
    f.write('#define __extension__\n')
    for header in headers:
        f.write(f'#include "{header}"\n')
    f.close()
    return fn


def filter_function(x):
    if hasattr(x, 'name'):
        if isinstance(x.name, str):
            return x.name.startswith('gtk_')
    return False


def get_pkgconfig_flags(pkg):
    return subprocess.check_output(['pkg-config', pkg, '--cflags']).decode().strip().split(' ')


def get_pkgconfig_libs(pkg):
    return subprocess.check_output(['pkg-config', pkg, '--libs']).decode().strip().split(' ')


def fix_lib(lib):
    if lib.startswith('-l'):
        return f'lib{lib[2:]}.so'


def compile_check(source_name, cpp_args):
    subprocess.check_output(['gcc', '-c', source_name] + cpp_args)


def go(args):
    file = generate_file_to_parse(args.header)
    cpp_args = []
    for pkg in args.pkg_config:
        cpp_args += get_pkgconfig_flags(pkg)

    text = subprocess.check_output(['cpp'] + cpp_args + [file]).decode()

    p = GnuCParser()
    ast = p.parse(text, filename='temp.h')
    top_level = ast.ext

    functions = map(FunctionDefinition,
                    filter(lambda node: isinstance(node, c_ast.Decl) and isinstance(node.type,
                                                                                    FuncDeclExt),
                           top_level))

    if len(args.regex):
        functions = filter(lambda func: any([re.match(r, func.name) for r in args.regex]),
                           functions)

    libs = args.lib or []
    for pkg in args.pkg_config:
        libs += get_pkgconfig_libs(pkg)

    libs = [fix_lib(lib) for lib in libs]

    loaded_libs = [ctypes.cdll.LoadLibrary(lib) for lib in libs]
    functions = filter(
        lambda func: any([hasattr(loaded_lib, func.name) for loaded_lib in loaded_libs]), functions)

    header_name = f'{args.name}_loader.h'
    source_name = f'{args.name}_loader.c'

    functions = list(functions)
    with open(header_name, 'w') as f:
        f.write(gen_header(args.name, args.header, functions))
    with open(source_name, 'w') as f:
        f.write(gen_source(args.name, functions, libs, header_name))

    compile_check(source_name, cpp_args)


def main():
    parser = argparse.ArgumentParser(description='dynimportgen')
    parser.add_argument('--pkg-config', '-p', type=str, nargs='+',
                        help='Use pkg-config to get a library')
    parser.add_argument('--header', '-H', type=str, nargs='+', help='Header to parse')
    parser.add_argument('--name', '-n', type=str, required=True, help='Name of the library')
    parser.add_argument('--regex', '-r', type=str, nargs='+', help='Filter functions by regex')
    parser.add_argument('--lib', '-l', type=str, nargs='+', help='Library files to load')

    args = parser.parse_args()
    go(args)


if __name__ == '__main__':
    main()
